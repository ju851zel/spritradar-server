package com.formigas.spritradar.config

import com.formigas.spritradar.provider.DatabaseProvider
import com.formigas.spritradar.provider.ProviderFirebase
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary

@Configuration
class ProviderConfiguration {
    val provider = ProviderFirebase()
//    val provider = ProviderInMemory()

    @Bean
    @Primary
    fun databaseProvider(): DatabaseProvider = provider

}
