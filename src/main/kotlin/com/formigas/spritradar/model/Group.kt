package com.formigas.spritradar.model

data class Group(
        val groupID: String,
        val memberIDs: List<String>,
        val ownerID: String,
        val creationDateIso: String,
)