package com.formigas.spritradar.controller

import com.formigas.spritradar.model.PetrolPrice
import com.formigas.spritradar.model.PetrolStation
import com.formigas.spritradar.model.Vehicle
import com.formigas.spritradar.service.DatabaseService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

import org.springframework.web.bind.annotation.CrossOrigin

@RestController
@CrossOrigin(origins = ["*"])
class RestController(private val dbService: DatabaseService) {

//    @PostMapping("/invalidate-cache")
//    fun invalidateCache() {
//        dbService.invalidateCache()
//    }

    @GetMapping("/stations")
    fun stations(@RequestParam("zip") zip: String?): List<PetrolStation> {
        return if (zip == null) dbService.getAllStations() else dbService.getEntries(zip)
    }

    @GetMapping("/prices")
    fun prices(@RequestParam("stationIds") stationIds: List<String>?): Map<String, PetrolPrice?> = if (stationIds == null) {
        dbService.getAllStations().associate { it.id to it.currentPrice }
    } else {
        dbService.getPrices(stationIds)
    }

    @PutMapping("/prices")
    fun prices(@RequestBody stations: Map<String, String>): Map<String, PetrolPrice?> {
        return dbService.getPrices(stations.keys.toList()).filter { it.value?.timestampIsoString != stations.get(it.key) }
    }

}
