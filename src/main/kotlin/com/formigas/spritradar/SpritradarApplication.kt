package com.formigas.spritradar

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
class SpritradarApplication

fun main(args: Array<String>) {
    runApplication<SpritradarApplication>(*args)

}
