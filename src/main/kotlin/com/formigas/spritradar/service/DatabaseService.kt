package com.formigas.spritradar.service

import com.formigas.spritradar.model.PetrolPrice
import com.formigas.spritradar.model.PetrolStation
import com.formigas.spritradar.model.Vehicle
import com.formigas.spritradar.provider.DatabaseProvider
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

@Service
class DatabaseService {
    @Autowired
    lateinit var provider: DatabaseProvider

    @Scheduled(fixedRate = 1000 * 60 * 60 * 24)
    fun fetchAllStationsAsync() = provider.fetchAllStationsAsync()

    @Scheduled(fixedRate = 1000 * 60 * 10)
    fun fetchAllPricesAsync() = provider.fetchAllPricesAsync()

    fun invalidateCache() = provider.invalidateCache()
    fun getEntries(zip: String): List<PetrolStation> = provider.getStationsByZip(zip)
    fun getAllStations(): List<PetrolStation> = provider.getAllStations()
    fun getPrices(stationIds: List<String>): Map<String, PetrolPrice?> = provider.getPrices(stationIds)
}

