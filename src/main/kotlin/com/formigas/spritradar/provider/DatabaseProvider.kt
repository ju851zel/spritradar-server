package com.formigas.spritradar.provider

import com.formigas.spritradar.model.PetrolPrice
import com.formigas.spritradar.model.PetrolStation
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

interface DatabaseProvider {
    fun invalidateCache()
    fun getStationsByZip(zip: String): List<PetrolStation>
    fun getAllStations(): List<PetrolStation>
    fun getPrices(stationIds: List<String>): Map<String, PetrolPrice?>

    fun fetchAllStationsAsync()
    fun fetchAllPricesAsync()

}
