package com.formigas.spritradar.provider

import com.formigas.spritradar.model.PetrolPrice
import com.formigas.spritradar.model.PetrolStation
import com.google.auth.oauth2.ServiceAccountCredentials
import com.google.cloud.firestore.Firestore
import com.google.cloud.firestore.FirestoreOptions
import com.google.cloud.storage.Storage
import com.google.cloud.storage.StorageOptions
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.ByteArrayInputStream
import kotlin.concurrent.thread

const val CLOUD_BUCKET_ENV_VAR = "CLOUD_BUCKET"
const val CLOUD_CREDENTIALS_ENV_VAR = "CLOUD_CREDENTIALS"

class ProviderFirebase : DatabaseProvider {
    private val firestore: Firestore
    private val storage: Storage

    private val CLOUD_BUCKET: String = System.getenv(CLOUD_BUCKET_ENV_VAR)
        ?: throw Exception("no cloud bucket env var specified for CLOUD_BUCKET")
    private val CREDENTIALS: String = System.getenv(CLOUD_CREDENTIALS_ENV_VAR)
        ?: throw Exception("no cloud bucket env var specified for CLOUD_CREDENTIALS")

    private var cache: Map<String, List<PetrolStation>>

    init {
        ServiceAccountCredentials.fromStream(CREDENTIALS.byteInputStream(Charsets.UTF_8)).also { credentials ->
            firestore = FirestoreOptions.newBuilder().setCredentials(credentials).build().service
            storage = StorageOptions.newBuilder().setCredentials(credentials).build().service
        }

        cache = mapOf()
    }

    override fun fetchAllStationsAsync() {
        thread {
            // TODO: Don't replace the cache but add only the new and updated stations
            cache = firestore.collection("stations").get().get().documents.map { data ->
                val json: String = Gson().toJson(data.data)
                val petrolType = object : TypeToken<PetrolStation>() {}.type
                return@map Gson().fromJson<PetrolStation>(json, petrolType)
            }.groupBy { it.zip }
        }
    }

    override fun fetchAllPricesAsync() {

        val bucket = storage.list().iterateAll().find { bucket -> bucket.name == CLOUD_BUCKET }
        val lastPrices = bucket?.list()?.iterateAll()?.maxByOrNull { it.name }
        if (lastPrices != null) {
            print(lastPrices.name)
            val petrolType = object : TypeToken<List<PetrolPrice>>() {}.type
            val stringContent: String = lastPrices.getContent().toString(Charsets.UTF_8)
            val prices: List<PetrolPrice> = Gson().fromJson(stringContent, petrolType)

            cache = cache.mapValues { (_, stations) ->
                stations.map { station ->
                    station.copy(currentPrice = prices.find { price ->
                        price.associatedStationId == station.id
                    })
                }
            }
        }
    }

    override fun invalidateCache() {
        cache = mapOf()
    }

    override fun getStationsByZip(zip: String): List<PetrolStation> = cache[zip] ?: listOf()

    override fun getAllStations(): List<PetrolStation> = cache.values.flatten()

    override fun getPrices(stationIds: List<String>): Map<String, PetrolPrice?> =
        getAllStations().filter { stationIds.contains(it.id) }.associate { it.id to it.currentPrice }

}

fun fromResources(relativePath: String): ByteArrayInputStream {
    try {
        return object {}.javaClass.classLoader.getResourceAsStream(relativePath).readAllBytes().inputStream()
    } catch (e: Exception) {
        throw Exception("Could not find file or read bytes of file=$relativePath")
    }
}
