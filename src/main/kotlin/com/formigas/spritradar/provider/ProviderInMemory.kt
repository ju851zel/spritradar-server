package com.formigas.spritradar.provider

import com.formigas.spritradar.model.PetrolPrice
import com.formigas.spritradar.model.PetrolStation
import com.formigas.spritradar.model.Vehicle
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.springframework.util.ResourceUtils
import java.nio.file.Files
import kotlin.concurrent.thread


class ProviderInMemory : DatabaseProvider {
    private var petrolStations: List<PetrolStation> = listOf()

    override fun invalidateCache() {}

    override fun getStationsByZip(zip: String): List<PetrolStation> = petrolStations.filter { it.zip == zip }

    override fun getAllStations(): List<PetrolStation> = petrolStations
    override fun getPrices(stationIds: List<String>): Map<String, PetrolPrice?> {
        TODO("Not yet implemented")
    }


    override fun fetchAllStationsAsync() {
        thread {
            val stationsFile = ResourceUtils.getFile("classpath:stations")
            val readBytes = String(Files.readAllBytes(stationsFile.toPath()));
            val petrolType = object : TypeToken<List<PetrolStation>>() {}.type
            petrolStations = Gson().fromJson(readBytes, petrolType)
        }
    }

    override fun fetchAllPricesAsync() {
        thread {
            val stationsFile = ResourceUtils.getFile("classpath:prices")
            val readBytes = String(Files.readAllBytes(stationsFile.toPath()));
            val petrolType = object : TypeToken<List<PetrolPrice>>() {}.type
            val prices: List<PetrolPrice> = Gson().fromJson(readBytes, petrolType)
            petrolStations = petrolStations.map { station ->
                station.copy(currentPrice = prices.find { price ->
                    price.associatedStationId == station.id
                })
            }
        }
    }

}
