import com.formigas.spritradar.SpritradarApplication
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest(
        classes = arrayOf(SpritradarApplication::class),
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
class SpritradarApplicationTest {

    @Autowired lateinit var testRestTemplate: TestRestTemplate
}
